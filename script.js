const sr = ScrollReveal();

sr.reveal('h5' , {
    origin: 'top',
    duration: 1600
});
sr.reveal('h2' , {
    origin: 'top',
    duration: 1600
});
sr.reveal('h1' , {
    origin: 'top',
    duration: 1600
});
sr.reveal('h3' , {
    origin: 'top',
    duration: 1600
});
sr.reveal('h4' , {
    origin: 'top',
    duration: 1600
});

sr.reveal('p' , {
    origin: 'top',
    duration: 1600
});

sr.reveal('.block' , {
  origin: 'top',    
  duration: 1600
});

sr.reveal('#img-project' , {
    origin: 'top',    
    duration: 1600,
    reset: true,
    interval: 200
  });

  sr.reveal('#flake' , {
    origin: 'top',    
    duration: 1600,
    reset: true,
    interval: 200
  });

  sr.reveal('#git' , {
    origin: 'top',    
    duration: 1600,
    reset: true,
    interval: 200
  });

  sr.reveal('#link' , {
    origin: 'top',    
    duration: 1600,
    reset: true,
    interval: 200
  });

  sr.reveal('#cv' , {
    origin: 'top',    
    duration: 1600,
    reset: true,
    interval: 200
  });
  sr.reveal('.progressBarContainer' , {
    origin: 'top',    
    duration: 1600
  });

 

